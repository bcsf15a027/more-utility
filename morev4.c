#include<stdio.h>
#include<stdlib.h>
#define PAGELEN 20
#define LINELEN 512
void do_more(FILE *);
int get_input(FILE *);


int main(int argc,char *argv[])
{
	if(argc==1)
	{
		do_more(stdin);
	}
	int i=0;
	FILE *fp;
		while(++i < argc)
		{
			fp = fopen(argv[i],"r");
			if(fp==NULL)
			{printf("Error in Opening File");exit(1);}
			do_more(fp);
			fclose(fp);
		}
	return 0;
}

int get_input(FILE *Stream)
{
	int c;
	printf("\033[7m --more--(%%) \033[m");
	
	c = getc(Stream);
	if(c == 'q')
		{return 0;}
	if(c == ' ')
		{return 1;}
	if(c == '\n')
		{return 2;}
	return 3;
}


void do_more(FILE *fp)
{
	char buffer[LINELEN];
	int RV;
	int NUM_OF_LINES =0;
	FILE *tty = fopen("/dev/tty","r");
	while(fgets(buffer,LINELEN,fp))
	{	
	
	fputs(buffer,stdout);
	NUM_OF_LINES++;

	if(NUM_OF_LINES == PAGELEN)
	{	
		RV=get_input(tty);	
		if(RV == 0)
		{	
		  	printf("\033[1A \033[2K \033[1G");
		  	break;
		  }
		  else if(RV == 1)
		  {
			printf("\033[1A \033[2K \033[1G");
		   	NUM_OF_LINES-= PAGELEN;
		  }

		else if(RV == 2)
		{

		    	printf("\033[1A \033[2K \033[1G");

			NUM_OF_LINES -= 1;


		  }
		  else if(RV == 3)
		{

			printf("\033[1A \033[2K \033[1G");
			break;
		 }
	}	
	}
}





